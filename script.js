const buffer = {
    value: 0,
    op: 0 // -1 odejmowanie, 0 nic, 1 dodawanie
};
class Calc {
    constructor() {
        this.value = 0;
        this.op = 0;

        this.container = document.createElement('DIV');
        this.container.id = 'container';

        this.screen = document.createElement('DIV');
        this.screen.id = 'display';
        this.screen.textContent = 0;
        this.container.appendChild(this.screen);

        [7, 8, 9, 'Clear', 4, 5, 6, '+', 1, 2, 3, '-', 0, '.', '='].forEach(item => {
            const button = document.createElement('DIV');
            button.textContent = item;
            button.addEventListener('click', event => this.mouseClick(event));

            if (item === 0) button.className = 'wide';
            this.container.appendChild(button);


        });



    }
    get screenValue() {
        return parseFloat(this.screen.textContent);
    }

    changeBuffer() {
        switch (this.op) {
            case -1:
                this.value -= this.screenValue;
                break;
            case 0:
                this.value = this.screenValue;
                break;
            case 1:
                this.value += this.screenValue;
                break;
        }
    }
    mouseClick(event) {
        const key = event.target.textContent;
        this.screen = document.getElementById('display');
        switch (key) {
            // clear
            case 'Clear':
                this.value = 0;
                this.op = 0;
                this.screen.textContent = 0; //clearuje wyswietlacz
                break;
            // .
            case '.':
                if (!this.screen.textContent.includes('.')) { //only jedna kropka allowed
                    this.screen.textContent += key;
                }
                break;
            // +
            case '+':
                this.changeBuffer();
                this.op = 1;
                this.screen.textContent = 0;
                break;
            // -
            case '-':
                this.changeBuffer();
                this.op = -1;
                this.screen.textContent = 0;
                break;
            // =
            case '=':
                this.changeBuffer();
                this.op = 0;
                this.screen.textContent = this.value;
                break;

            default:
                if (this.screen.textContent === '0') { //jesli jest pierwsze 0 to zamienia na wprowadzony znak
                    this.screen.textContent = key;   // zmienia na key
                }
                else {
                    this.screen.textContent += key; //dodaje key
                }
                break;
        }
    }

    init() {
        // const content = document.getElementById('container');
        // const buttons = content.children;
        // for (let i = 1; i < buttons.length; ++i) {
        //     buttons[i].addEventListener('click',event => this.mouseClick(event));
        // }

        document.body.appendChild(this.container);
    }

}

const calc = new Calc();


window.addEventListener('DOMContentLoaded', () => calc.init());